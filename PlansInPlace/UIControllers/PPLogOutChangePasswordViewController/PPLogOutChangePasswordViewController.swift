//
//  ChangePasswordViewController.swift
//  PlansInPlace
//
//  Created by User on 29/09/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

class PPLogOutChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        hideKeyboardWhenTappedAround()

    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PPLogOutChangePasswordViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        newPasswordTextField.endEditing(true)
        confirmPasswordTextField.endEditing(true)
    }
    
    @IBAction func saveAction(sender: UIButton) {
        //TODO: save password method
    }
}
