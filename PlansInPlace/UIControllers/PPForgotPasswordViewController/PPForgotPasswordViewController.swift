//
//  ForgotPasswordViewController.swift
//  PlansInPlace
//
//  Created by User on 29/09/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

class PPForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var codeTextField: UITextField!
    
    override func viewDidLoad() {
        hideKeyboardWhenTappedAround()

    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PPForgotPasswordViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        codeTextField.endEditing(true)
    }
    
    @IBAction func sendAgainCodeAction(sender: UIButton) {
        //TODO: send code again method
    }
    
    @IBAction func checkCodeAction(sender: UIButton) {
        //TODO: check code method
    }
}