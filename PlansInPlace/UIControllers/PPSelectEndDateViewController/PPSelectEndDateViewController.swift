//
//  PPSelectEndDateViewController.swift
//  PlansInPlace
//
//  Created by User on 12/10/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

protocol PPSelectEndDateViewControllerDelegate {
    
    func selectEndDate(controller: PPSelectEndDateViewController, text: String)
}

class PPSelectEndDateViewController: UIViewController {
    
    var delegate: PPSelectTypeViewControllerDelegate! = nil
    
    @IBOutlet weak var endDatePicker: UIDatePicker!
    
    @IBAction func backButtonAction(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func saveButtonAction(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}