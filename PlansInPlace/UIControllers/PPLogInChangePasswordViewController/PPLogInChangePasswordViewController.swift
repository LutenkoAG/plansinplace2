//
//  PPLogInChangePasswordViewController.swift
//  PlansInPlace
//
//  Created by User on 03/10/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

class PPLogInChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        hideKeyboardWhenTappedAround()
        
    }
    
    @IBAction func backButtonAction(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PPLogInChangePasswordViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        newPasswordTextField.endEditing(true)
        confirmPasswordTextField.endEditing(true)
    }
    
    @IBAction func saveAction(sender: UIButton) {
        //TODO: save password method
    }
}