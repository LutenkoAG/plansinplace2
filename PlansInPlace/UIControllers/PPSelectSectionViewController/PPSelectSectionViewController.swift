//
//  PPSelectSectionTableViewController.swift
//  PlansInPlace
//
//  Created by User on 05/10/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

protocol PPSelectSectionViewControllerDelegate {
    
    func selectSection(controller: PPSelectSectionViewController, text: String)
}

class PPSelectSectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var delegate:PPSelectSectionViewControllerDelegate! = nil
    
    var sectionNames = [String]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        sectionNames = ["Personal","Legal","Financial","Insurance","Tax","Health","Real estate","Retirement","Military"]
    }
    
    @IBAction func backAction(sender: UIBarButtonItem) {
       self.navigationController?.popViewControllerAnimated(false)

    }
 
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionNames.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SelectedTrunkSectionCell", forIndexPath: indexPath) as! PPSelectSectionTableViewCell
        
        cell.trunkSectionLabel.text = sectionNames[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        delegate!.selectSection(self, text: sectionNames[indexPath.row])
        self.navigationController?.popViewControllerAnimated(true)
    }
}