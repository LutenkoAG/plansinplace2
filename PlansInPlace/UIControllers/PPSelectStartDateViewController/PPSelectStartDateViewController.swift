//
//  PPSelectStartDateViewController.swift
//  PlansInPlace
//
//  Created by User on 12/10/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

class PPSelectStartDateViewController: UIViewController {
    
    @IBOutlet weak var startDatePicker: UIDatePicker!
    
    @IBAction func backButtonAction(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func saveButtonAction(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}