//
//  PPSelectTypeTableViewController.swift
//  PlansInPlace
//
//  Created by User on 10/10/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

protocol PPSelectTypeViewControllerDelegate {
    func selectType(controller: PPSelectTypeViewController)
}

class PPSelectTypeViewController: UIViewController {
    
    var delegate: PPSelectTypeViewControllerDelegate! = nil
    
    @IBAction func backButtonAction(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}