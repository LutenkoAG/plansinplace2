//
//  LoginViewController.swift
//  PlansInPlace
//
//  Created by User on 28/09/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

class PPLoginViewController: UIViewController {
   
    @IBOutlet weak var emeilTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        hideKeyboardWhenTappedAround()
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PPLoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        emeilTextField.endEditing(true)
        passwordTextField.endEditing(true)
    }
    
    @IBAction func loginAction(sender: AnyObject) {
        // TODO: login method
    }

    @IBAction func forgotPasswordAction(sender: UIButton) {
        // TODO: forgot password method
    }
    
}
