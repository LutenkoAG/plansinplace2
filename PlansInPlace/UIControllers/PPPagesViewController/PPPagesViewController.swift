//
//  PPPagesViewController.swift
//  PlansInPlace
//
//  Created by User on 06/10/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

class PPPagesViewController: UIViewController {
    
    var pagesArray = [UIImage]()
    
    @IBOutlet weak var pageImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
    @IBAction func backButtonAction(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func addPagesAction(sender: UIButton) {
        
    }
    
}
