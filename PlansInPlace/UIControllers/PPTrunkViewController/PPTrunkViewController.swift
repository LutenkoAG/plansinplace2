//
//  TrunkViewController.swift
//  PlansInPlace
//
//  Created by User on 30/09/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

class PPTrunkViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var nopeButton: UIBarButtonItem!
    
     var imageArray = [String]()
    
    @IBOutlet weak var trunkCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        imageArray = ["Personal","Legal","Financial","Insurance","Tax","Health","Real estate","Retirement","Military"]

        self.plusButton?.clipsToBounds = true
        self.plusButton!.layer.cornerRadius = 20
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        nopeButton.enabled = false
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("TrunkItemCell", forIndexPath: indexPath) as! PPTrunkCollectionViewCell
        let image : UIImage = UIImage(named: imageArray[indexPath.row])!
        cell.trunkSectionImageView.image = image
        return cell
    }
    
}