//
//  PPTrunkCollectionViewCell.swift
//  PlansInPlace
//
//  Created by User on 03/10/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

class PPTrunkCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var trunkSectionImageView: UIImageView!
    
}