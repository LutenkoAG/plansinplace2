//
//  PPSettingsViewController.swift
//  PlansInPlace
//
//  Created by User on 03/10/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

class PPSettingsViewController: UIViewController {
    
    @IBOutlet weak var firsrtNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    override func viewDidLoad() {
        hideKeyboardWhenTappedAround()
        
    }
    
    @IBAction func backButtonAction(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PPSettingsViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        firsrtNameTextField.endEditing(true)
        lastNameTextField.endEditing(true)
    }
    
    @IBAction func saveNamesAction(sender: UIBarButtonItem) {
        //TODO: save names mathod
    }
    
    @IBAction func logOutAction(sender: UIButton) {
        //TODO: log out method
    }
}