//
//  PPDocumentSettingViewController.swift
//  PlansInPlace
//
//  Created by User on 04/10/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

class PPDocumentSettingViewController: UIViewController, PPSelectSectionViewControllerDelegate {
    
    @IBOutlet weak var countPagesDocumentLabel: UILabel!
    @IBOutlet weak var dokumentTitleTextField: UITextField!
    @IBOutlet weak var trunkSectionTextField: UITextField!
    @IBOutlet weak var documentTypeTextField: UITextField!
    @IBOutlet weak var documentDescriptionTextField: UITextField!
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var provideAccessToView: UIView!
    
    var trunkSection: String = ""
    var documentType: String = ""
    
    
    override func viewDidLoad() {
        hideKeyboardWhenTappedAround()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        trunkSectionTextField.text = trunkSection
    }
    
    func selectSection(controller: PPSelectSectionViewController, text: String) {
        trunkSection = text
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SelectSectionSegue" {
            let vc = segue.destinationViewController as! PPSelectSectionViewController
            vc.delegate = self
        }
    }
    
    @IBAction func provideAccessToAction(sender: UIButton) {
        provideAccessToView.hidden = false
    }
    
    @IBAction func okProvideAccessToAction(sender: UIButton) {
        provideAccessToView.hidden = true
    }
    
    @IBAction func backButtonAction(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func addToTrunkAction(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("navController")
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PPDocumentSettingViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        dokumentTitleTextField.endEditing(true)
        documentDescriptionTextField.endEditing(true)
    }
}