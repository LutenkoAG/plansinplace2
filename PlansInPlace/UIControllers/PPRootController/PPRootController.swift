//
//  RootController.swift
//  PlansInPlace
//
//  Created by User on 28/09/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

class PPRootViewController: UIViewController {
    
    var counter = 0
    var timer = NSTimer()
    
    override func viewDidLoad() {
        timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(PPRootViewController.timerAction), userInfo: nil, repeats: true)
    }
    
    func timerAction() {
        counter += 1
        if (counter == 5) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("LoginScreen") 
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
}