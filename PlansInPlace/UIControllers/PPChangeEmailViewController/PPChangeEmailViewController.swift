//
//  PPChangeEmailViewController.swift
//  PlansInPlace
//
//  Created by User on 03/10/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

import Foundation
import UIKit

class PPChangeEmailViewController: UIViewController {
    
    @IBOutlet weak var emaillLabel: UILabel!
    @IBOutlet weak var newEmailTextField: UITextField!
    
    override func viewDidLoad() {
        hideKeyboardWhenTappedAround()
        
    }
    
    @IBAction func backButtonAction(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PPChangeEmailViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        newEmailTextField.endEditing(true)
    }
    
    @IBAction func saveAction(sender: UIButton) {
        //TODO: save email method
    }
}